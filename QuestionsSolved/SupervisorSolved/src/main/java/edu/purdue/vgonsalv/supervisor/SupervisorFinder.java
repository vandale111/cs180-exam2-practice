/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.supervisor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 *
 * @author Victor
 */
public class SupervisorFinder {
    private final List<Employee> employees = new ArrayList<>();
    
    private Employee findEmployee(String name) {
        for (Employee employee : employees) {
            if(employee.getName().equals(name)){
                return employee;
            }
        }
        return null;
    }
    private Employee createIfNotExists(String name) {
        Employee employee = findEmployee(name);
        if(employee == null){
            employee = new Employee(name);
            employees.add(employee);
        }
        return employee;
    }
    
    
    public  static SupervisorFinder load(Reader in) throws IOException{
        SupervisorFinder sf = new SupervisorFinder();
        try(BufferedReader br = new BufferedReader(in)){
            String line;
            while((line = br.readLine())!=null){
                String[] names = line.split(",");
                Employee employee = sf.createIfNotExists(names[0]);
                if(names.length >1){
                    Employee supervisor = sf.createIfNotExists(names[1]);
                    employee.setSupervisor(supervisor);
                    supervisor.addSupervisee(employee);
                }
            }
        }
        return sf;
    }
    public String toString(){
        StringJoiner sj = new StringJoiner(",", "{", "}");
        for (Employee employee : employees) {
            sj.add(employee.toString());
        }
        return sj.toString();
    }
}
