/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.supervisor;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 *
 * @author Victor
 */
public class Employee {
    private final String name;
    private Employee supervisor;
    private final List<Employee> supervisees;

    public Employee(String name) {
        this.supervisees = new ArrayList<>();
        this.name = name;
    }

    public Employee getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Employee supervisor) {
        this.supervisor = supervisor;
    }

    public String getName() {
        return name;
    }
    
    public void addSupervisee(Employee e) {
        supervisees.add(e);
    }

    public List<Employee> getSupervisees() {
        return supervisees;
    }

    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner("\",\"",
                String.format("\"%s\":[\"", name),
                "\"]");
        for (Employee supervisee : supervisees) {
            sj.add(supervisee.name);
        }
        return sj.toString();
    }
    
    
    
}
