/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.supervisor;

import java.io.IOException;
import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

/**
 *
 * @author Victor
 */
public class SupervisorFinderTest {
    SupervisorFinder sf;
    
        String ls = System.lineSeparator();
    JsonObjectBuilder expected;
    public SupervisorFinderTest() {
        expected = Json.createObjectBuilder();
    }
    
    private void addEntry(String supervisor, String... supervised) {
        JsonArrayBuilder jsab = Json.createArrayBuilder();
        for (String string : supervised) {
            jsab.add(string);
        }
        expected.add(supervisor, jsab);
    }
    
    @Test
    public void testBasic() throws IOException, JSONException {
        String inputData = "a";
        String result = SupervisorFinder.load(new StringReader(inputData)).toString();
        addEntry("a","");
        String exp = expected.build().toString();
        JSONAssert.assertEquals(exp, result, JSONCompareMode.NON_EXTENSIBLE);
    }
    @Test
    public void test1Supervispr() throws IOException, JSONException {
        String inputData = "a,b"+ls+"b";
        String result = SupervisorFinder.load(new StringReader(inputData)).toString();
        addEntry("b","a");
        addEntry("a","");
        String exp = expected.build().toString();
        JSONAssert.assertEquals(exp, result, JSONCompareMode.NON_EXTENSIBLE);
    }
    @Test
    public void test1Supervisor2() throws IOException, JSONException {
        String inputData = "a,b"+ls + "b"+ls+"c,b";
        String result = SupervisorFinder.load(new StringReader(inputData)).toString();
        addEntry("b","a","c");
        addEntry("c","");
        addEntry("a","");
        String exp = expected.build().toString();
        JSONAssert.assertEquals(exp, result, JSONCompareMode.NON_EXTENSIBLE);
    }
    @Test
    public void testMultipleSupervisors() throws IOException, JSONException {
        String inputData = "a,b"+ls + "b"+ls+"c,b"+ls+"x,d"+ls+"y,d"+ls+"f,a"+ls+"d";
        String result = SupervisorFinder.load(new StringReader(inputData)).toString();
        addEntry("b","a","c");
        addEntry("c","");
        addEntry("a","f");
        addEntry("d","x","y");
        addEntry("x","");
        addEntry("y","");
        addEntry("f","");
        String exp = expected.build().toString();
        JSONAssert.assertEquals(exp, result, JSONCompareMode.NON_EXTENSIBLE);
    }    
}
