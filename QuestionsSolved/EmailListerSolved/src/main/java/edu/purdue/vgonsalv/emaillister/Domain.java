/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.emaillister;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 *
 * @author Victor
 */
public class Domain {
    private final String name;
    private final List<String> emails;

    public Domain(String name) {
        this.emails = new ArrayList<>();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<String> getEmails() {
        return emails;
    }
    
    public void addEmail(String email) {
        emails.add(email);
    }
    
    public String toString() {
        String header = String.format("\"%s\":[\"", name);
        StringJoiner sj = new StringJoiner("\",\"", header, "\"]");
        for (String email : emails) {
            sj.add(email);
        }
        return sj.toString();
    }
    
}
