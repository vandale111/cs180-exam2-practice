/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.emaillister;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 *
 * @author Victor
 */
public class EmailLister {

    List<Domain> domains = new ArrayList<>();

    private Domain findDomain(String name) {
        for (Domain domain : domains) {
            if(domain.getName().equals(name)){
                return domain;
            }
        }
        return null;
    }
    private Domain creatIfNotExists(String name) {
        Domain domain = findDomain(name);
        if(domain == null){
            domain = new Domain(name);
            domains.add(domain);
        }
        return domain;
    }
    
    public static EmailLister load(Reader in) throws IOException {
        EmailLister el = new EmailLister();
        try (BufferedReader br = new BufferedReader(in)) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split("@");
                Domain domain = el.creatIfNotExists(parts[1]);
                domain.addEmail(line);
            }
        }
        return el;
    }
    @Override
    public String toString(){
        StringJoiner sj = new StringJoiner(",", "{", "}");
        for (Domain domain : domains) {
            sj.add(domain.toString());
        }
        return sj.toString();
    }
}
