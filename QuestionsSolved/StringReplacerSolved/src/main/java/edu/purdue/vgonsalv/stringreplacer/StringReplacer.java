/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.stringreplacer;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Victor
 */
public class StringReplacer {

    List<Rule> rules = new ArrayList<>();

    private Rule findRule(char target) {
        for (Rule rule : rules) {
            if (rule.getTarget() == target) {
                return rule;
            }
        }
        return null;
    }

    public void addRule(char target, String replacement) {
        Rule old = findRule(target);
        if(old ==null){
            rules.add(new Rule(target, replacement));
        }else{
            old.setReplacement(replacement);
        }
    }
    
    public String replace(String str) {
        StringBuilder sb = new StringBuilder(str.length());
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char c = str.charAt(i);
            Rule r = findRule(c);
            if(r!= null){
                sb.append(r.getReplacement());
            }else{
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
