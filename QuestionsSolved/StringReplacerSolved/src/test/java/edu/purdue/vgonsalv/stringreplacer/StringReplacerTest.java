/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.stringreplacer;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Victor
 */
public class StringReplacerTest {
    StringReplacer sr = new StringReplacer();
    public StringReplacerTest() {
    }

    @Test
    public void testNoRules() {
        String result =    sr.replace("hello world");
        assertEquals("Test that your code works with no rules", "hello world", result);
    }
    @Test
    public void testNoMatchingRules() {
        sr.addRule('a', "foo bar");
        sr.addRule('b', "foo bar");
        String result =    sr.replace("hello world");
        assertEquals("Test that your code works with no matching rules", "hello world", result);
    }
    @Test
    public void testSimpleRule() {
        sr.addRule('a', "aa");
        String result =    sr.replace("a");
        assertEquals("aa", result);
    }
    @Test
    public void testPythagorasRules() {
        sr.addRule('1', "11");
        sr.addRule('0', "1[0]0");
        String result = sr.replace("0");
        assertEquals("1[0]0", result);
        result = sr.replace(result);
        assertEquals("11[1[0]0]1[0]0", result);
        result = sr.replace(result);
        assertEquals("1111[11[1[0]0]1[0]0]11[1[0]0]1[0]0", result);
    }
    @Test
    public void testSierpinskiRules() {
        sr.addRule('A', "+B-A-B+");
        sr.addRule('B', "-A+B+A-");
        String result = sr.replace("A");
        assertEquals("+B-A-B+", result);
        result = sr.replace(result);
        assertEquals("+-A+B+A--+B-A-B+--A+B+A-+", result);
        result = sr.replace(result);
        assertEquals("+-+B-A-B++-A+B+A-++B-A-B+--+-A+B+A--+B-A-B+--A+B+A-+--+B-A-B++-A+B+A-++B-A-B+-+", result);
    }
    @Test
    public void testDragonRules() {
        sr.addRule('X', "X+YF+");
        sr.addRule('Y', "-FX-Y");
        String result = sr.replace("FX");
        assertEquals("FX+YF+", result);
        result = sr.replace(result);
        assertEquals("FX+YF++-FX-YF+", result);
        result = sr.replace(result);
        assertEquals("FX+YF++-FX-YF++-FX+YF+--FX-YF+", result);
    }
    @Test
    public void testNewRule() {
        sr.addRule('a', "aa");
        sr.addRule('a', "bb");
        String result = sr.replace("a");
        assertEquals("Check that new rule replace old ones","bb",result);
    }
    
    
}
