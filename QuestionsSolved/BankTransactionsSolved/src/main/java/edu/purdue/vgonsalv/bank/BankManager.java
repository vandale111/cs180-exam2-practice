/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.bank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vgonsalv
 */
public class BankManager {
    private final List<BankAccount> accounts = new ArrayList<>();
    
    private BankAccount findAccount(String name) {
        for (BankAccount account : accounts) {
            if(account.getOwner().equals(name)){
                return account;
            }
        }
        return null;
    }
    
    private void createNewAccount(String name, BigDecimal initialBalance) {
        accounts.add(new BankAccount(name, initialBalance));
    }
    private void depositIntoAccount(String name, BigDecimal amount) {
        findAccount(name).deposit(amount);
    }
    private void withdrawFromAccount(String name, BigDecimal amount) {
        findAccount(name).withdraw(amount);
    }
    private void transfer(String source, String destination, BigDecimal amount) {
        BankAccount src = findAccount(source);
        if(src.withdraw(amount)){
            BankAccount dest = findAccount(destination);
            dest.deposit(amount);
        }
    }
    
    public void readData(InputStream in) throws IOException {
        accounts.clear();
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(in))) {
            String line;
            while((line = br.readLine())!= null){
                String[] tokens = line.split(",");
                switch(tokens[0]){
                    case "CREATE": //CREATE,<owner>,<balance>
                        createNewAccount(tokens[1], new BigDecimal(tokens[2]));
                        break;
                    case "DEPOSIT"://DEPOSIT,<owner>,<amount>
                        depositIntoAccount(tokens[1],new BigDecimal(tokens[2]));
                        break;
                    case "WITHDRAW"://WITHDRAW,<owner>,<amount>
                        withdrawFromAccount(tokens[1],new BigDecimal(tokens[2]));
                        break;
                    case "TRANSFER"://TRANSFER,<owner>,<amount>,<destinantion account>
                        transfer(tokens[1],tokens[3],new BigDecimal(tokens[2]));
                        break;
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (BankAccount account : accounts) {
            result.append(String.format("%s,%.2f%n", account.getOwner(),account.getBalance()));
            
        }
        return result.toString();
    }
}
