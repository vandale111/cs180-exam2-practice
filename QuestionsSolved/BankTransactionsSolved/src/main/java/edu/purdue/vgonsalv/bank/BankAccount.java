/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.bank;

import java.math.BigDecimal;

/**
 *
 * @author vgonsalv
 */
public class BankAccount {

    private final String owner;
    private BigDecimal balance;
    
    public BankAccount(String owner, BigDecimal initialBalance) {
        if (owner == null) {
            throw new IllegalArgumentException("Owner cannot be null");
        }
        if (initialBalance.signum() < 0) {
            throw new IllegalArgumentException("Balance cannot be negative");
        }
        this.owner = owner;
        this.balance = initialBalance;
    }

    public String getOwner() {
        return owner;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void deposit(BigDecimal amount) {
        assert (amount.signum() >= 0);
        assert (balance.signum() >= 0);
        balance = balance.add(amount);
    }

    public boolean withdraw(BigDecimal amount) {
        assert (amount.signum() >= 0);
        assert (balance.signum() >= 0);
        
        BigDecimal tempBalance = balance.subtract(amount);
        if (tempBalance.signum() >= 0) {
            balance = tempBalance;
            return true;
        }
        return false;
    }

}
