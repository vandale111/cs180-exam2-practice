/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.bank;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author vgonsalv
 */
public class BankManagerTest {

    BankManager bm;

    public BankManagerTest() {
        bm = new BankManager();
    }

    private static class TestBuilder {

        private final StringBuilder testCase = new StringBuilder();

        public TestBuilder createAccount(String name, String initialBalance) {
            testCase.append("CREATE,").append(name).append(",")
                    .append(initialBalance).append(System.lineSeparator());
            return this;
        }

        public TestBuilder deposit(String name, String amount) {
            testCase.append("DEPOSIT,").append(name).append(",")
                    .append(amount).append(System.lineSeparator());
            return this;
        }

        public TestBuilder withdraw(String name, String amount) {
            testCase.append("WITHDRAW,").append(name)
                    .append(",").append(amount).append(System.lineSeparator());
            return this;
        }

        public TestBuilder transfer(String source, String destination, String amount) {
            testCase.append("TRANSFER,").append(source).append(",")
                    .append(amount).append(",")
                    .append(destination).append(System.lineSeparator());
            return this;
        }

        public InputStream getStream() {
            return new ByteArrayInputStream(testCase.toString().getBytes(Charset.forName("UTF-8")));
        }
    }

    private static class ResultBuilder {

        List<String> lines = new ArrayList<>();

        private ResultBuilder createAccount(String name, String balance) {
            lines.add(name + "," + balance);
            return this;
        }

        public String[] getLines() {
            return lines.toArray(new String[lines.size()]);
        }

    }

    @Test(timeout = 100)
    public void testCreate() throws IOException {
        InputStream testCase = new TestBuilder()
                .createAccount("John Doe", "100.00")
                .getStream();

        bm.readData(testCase);
        Assert.assertArrayEquals("check that basic create statemnet work and format is correct",
                new ResultBuilder().createAccount("John Doe", "100.00").getLines(),
                bm.toString().split("\r?\n"));
    }

    @Test(timeout = 100)
    public void testFormat() throws IOException {
        InputStream testCase = new TestBuilder()
                .createAccount("John Doe", "100.00")
                .createAccount("Jane Doe", "130.50")
                .createAccount("Luke Skywalker", "350.00")
                .createAccount("Bill Gate", "75000000000.00")
                .getStream();

        bm.readData(testCase);
        Assert.assertArrayEquals("check that formating is correct",
                new ResultBuilder()
                .createAccount("John Doe", "100.00")
                .createAccount("Jane Doe", "130.50")
                .createAccount("Luke Skywalker", "350.00")
                .createAccount("Bill Gate", "75000000000.00")
                .getLines(),
                bm.toString().split("\r?\n"));
    }

    @Test//(timeout = 100)
    public void testWithdraw() throws IOException {
        InputStream testCase = new TestBuilder()
                .createAccount("John Doe", "100.00")
                .createAccount("Jane Doe", "130.50")
                .withdraw("John Doe", "40")
                .withdraw("Jane Doe", "30")
                .getStream();

        bm.readData(testCase);
        Assert.assertArrayEquals("check that withdraw is correct",
                new ResultBuilder()
                .createAccount("John Doe", "60.00")
                .createAccount("Jane Doe", "100.50")
                .getLines(),
                bm.toString().split("\r?\n"));
    }

    @Test(timeout = 100)
    public void testDeposit() throws IOException {
        InputStream testCase = new TestBuilder()
                .createAccount("John Doe", "100.00")
                .createAccount("Jane Doe", "130.50")
                .deposit("John Doe", "40")
                .deposit("Jane Doe", "30")
                .getStream();

        bm.readData(testCase);
        Assert.assertArrayEquals("check that depost is correct",
                new ResultBuilder()
                .createAccount("John Doe", "140.00")
                .createAccount("Jane Doe", "160.50")
                .getLines(),
                bm.toString().split("\r?\n"));
    }
    @Test(timeout = 100)
    public void testTransfer() throws IOException {
        InputStream testCase = new TestBuilder()
                .createAccount("John Doe", "100.00")
                .createAccount("Jane Doe", "130.50")
                .transfer("John Doe", "Jane Doe", "30")
                .getStream();

        bm.readData(testCase);
        Assert.assertArrayEquals("check that transfer is correct",
                new ResultBuilder()
                .createAccount("John Doe", "70.00")
                .createAccount("Jane Doe", "160.50")
                .getLines(),
                bm.toString().split("\r?\n"));
    }
    @Test(timeout = 100)
    public void testNonNegative() throws IOException {
        InputStream testCase = new TestBuilder()
                .createAccount("John Doe", "100.00")
                .createAccount("Jane Doe", "130.50")
                .withdraw("John Doe", "200")
                .transfer("John Doe", "Jane Doe", "300")
                .getStream();

        bm.readData(testCase);
        Assert.assertArrayEquals("check that bank acounts cannot go into the negative",
                new ResultBuilder()
                .createAccount("John Doe", "100.00")
                .createAccount("Jane Doe", "130.50")
                .getLines(),
                bm.toString().split("\r?\n"));
    }

    @Test(timeout = 100)
    public void testReset() throws IOException {
        InputStream testCase = new TestBuilder()
                .createAccount("John Doe", "100.00")
                .getStream();

        bm.readData(testCase);
        testCase = new TestBuilder()
                .createAccount("Jane Doe", "200.00")
                .getStream();

        bm.readData(testCase);
        Assert.assertArrayEquals("check that basic read resets data between invocatiosn",
                new ResultBuilder().createAccount("Jane Doe", "200.00").getLines(),
                bm.toString().split("\r?\n"));
    }

}
