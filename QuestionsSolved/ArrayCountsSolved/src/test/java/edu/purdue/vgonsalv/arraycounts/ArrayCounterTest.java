/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.arraycounts;

import java.io.IOException;
import java.io.StringReader;
import java.util.StringJoiner;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Victor
 */
public class ArrayCounterTest {
    ArrayCounter ac;
    public ArrayCounterTest() {
        ac = new ArrayCounter();
    }

    @Test(timeout = 100)
    public void testBasic() throws IOException {
        String test = "0,0,1";
        ac.load(new StringReader(test));
        int result = ac.getTotal(0, 0);
        assertEquals("check basic implentation works", 1, result);
    }
    @Test(timeout = 100)
    public void test1() throws IOException {
        String test = "0,100,1";
        ac.load(new StringReader(test));
        int result = ac.getTotal(20, 50);
        assertEquals("check basic implentation works", 31, result);
    }
    @Test(timeout = 100)
    public void test2() throws IOException {
        String test = "0,100,1";
        ac.load(new StringReader(test));
        int result = ac.getTotal(200, 300);
        assertEquals("check for reads outside range", 0, result);
    }
    @Test(timeout = 100)
    public void test3() throws IOException {
        String test = "0,100,1";
        ac.load(new StringReader(test));
        int result = ac.getTotal(50, 300);
        assertEquals("check for reads that straddle range", 51, result);
    }
    @Test(timeout = 100)
    public void test4() throws IOException {
        StringJoiner test = new StringJoiner(System.lineSeparator());
        test.add("0,100,1");
        test.add("5,10000,10");
        
        ac.load(new StringReader(test.toString()));
        int result = ac.getTotal(0, 9000);
        assertEquals("check for big range", 8996 *10 + 101, result);
    }
    @Test(timeout = 100)
    public void test5() throws IOException {
        StringJoiner test = new StringJoiner(System.lineSeparator());
        test.add("0,100,1");
        test.add("9000,10000,10");
        
        ac.load(new StringReader(test.toString()));
        int result = ac.getTotal(0, 9000);
        assertEquals("check for when new data starts outside range", 10 + 101, result);
    }
    @Test(timeout = 100)
    public void test6() throws IOException {
        StringJoiner test = new StringJoiner(System.lineSeparator());
        test.add("0,100,-100");
        test.add("0,100,10");
        
        ac.load(new StringReader(test.toString()));
        int result = ac.getTotal(0, 9000);
        assertEquals("check that negative numbers work", -90*101, result);
    }
    
}
