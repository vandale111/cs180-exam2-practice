/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.arraycounts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;

/**
 *
 * @author Victor
 */
public class ArrayCounter {
    private int[] data = new int[10];
    
    private void ensureSize(int len) {
        int newLen = data.length;
        if(newLen <len){
            do{
                newLen *= 2;
            }while(newLen < len);
            data = Arrays.copyOf(data, newLen);
        }
    }
    private void increment(int from, int to, int amount) {
        ensureSize(Math.max(from,to));
        for (int i = from; i <= to; i++) {
            data[i] += amount;
        }
    }
    public int getTotal(int from, int to) {
        if(from >= data.length){
            return 0;
        }
        if(to >= data.length){
            to = data.length -1;
        }
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum += data[i];
        }
        return sum;
    }
    public void load(Reader r)throws IOException{
        try(BufferedReader br = new BufferedReader(r)){
            String line;
            while((line = br.readLine())!= null){
                String[] parts = line.split(",");
                increment(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), Integer.parseInt(parts[2]));
            }
        }
    }
}
