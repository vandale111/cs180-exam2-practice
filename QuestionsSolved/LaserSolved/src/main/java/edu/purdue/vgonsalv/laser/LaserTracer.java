/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.laser;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Victor
 */
public class LaserTracer {
    private static final int UP=0;
    private static final int RIGHT=1;
    private static final int DOWN=2;
    private static final int LEFT=3;
    
    private final List<Mirror> mirrors ;
    private final int rows;
    private final int cols;

    public LaserTracer(List<Mirror> mirrors, int rows, int cols) {
        this.mirrors = mirrors;
        this.rows = rows;
        this.cols = cols;
    }
    
    private Mirror getAtPos(Point pos){
        for (Mirror mirror : mirrors) {
            if(mirror.getPosition().equals(pos)){
                return mirror;
            }
        }
        return null;
    }
    public Point getCollision(Point pos, int direction){
        while(pos.x >0 && pos.x < cols-1 && pos.y >0 && pos.y < rows-1 ){
            switch(direction){
                case UP:
                    pos.y--;
                    break;
                case RIGHT:
                    pos.x++;
                    break;
                case DOWN:
                    pos.y++;
                    break;
                case LEFT:
                    pos.x--;
                    break;
            }
            Mirror mirror = getAtPos(pos);
            if(mirror!= null){
                if(mirror.getDirection()=='/'){
                    switch(direction){
                        case UP:
                            direction = RIGHT;
                            break;
                        case RIGHT:
                            direction = UP;
                            break;
                        case DOWN:
                            direction = LEFT;
                            break;
                        case LEFT:
                            direction = DOWN;
                            break;
                    }
                }else{
                    switch(direction){
                        case UP:
                            direction = LEFT;
                            break;
                        case RIGHT:
                            direction = DOWN;
                            break;
                        case DOWN:
                            direction = RIGHT;
                            break;
                        case LEFT:
                            direction = UP;
                            break;
                    }
                }
            }
        }
        return pos;
    }

    public static LaserTracer load(Reader text) throws IOException {
        List<Mirror> mirrors;
        int cols;
        int rows;
        try (BufferedReader br = new BufferedReader(text)) {
            String firstLine = br.readLine();
            mirrors = new ArrayList<>();
            

            rows = 1;
            cols = firstLine.length() ;
            String line;
            
            while((line = br.readLine())!=null){
                for(int i =1; i< cols -1; i++){
                    switch(line.charAt(i)){
                        case '/':
                        case '\\':
                            mirrors.add(new Mirror(new Point(rows,i), line.charAt(i)));
                    }
                }
                rows++;                
            }
        }
        return new LaserTracer(mirrors, rows, cols);
    }

}
