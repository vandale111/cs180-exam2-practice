/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.laser;

import java.awt.Point;

/**
 *
 * @author Victor
 */
public class Mirror {
    private final Point position;
    private final char direction;

    public Mirror(Point position, char direction) {
        if(position.x <0 || position.y < 0){
            throw new IllegalArgumentException(String.format("position cannot have negative coordinates (%d,%d)", position.x,position.y));
        }
        if(direction != '/' && direction != '\\'){
            throw new IllegalArgumentException(String.format("invalid direction, must be '\\' or '/': '%c'", direction));
        }
        this.position = position;
        this.direction = direction;
    }

    public Point getPosition() {
        return position;
    }

    public char getDirection() {
        return direction;
    }
    
}
