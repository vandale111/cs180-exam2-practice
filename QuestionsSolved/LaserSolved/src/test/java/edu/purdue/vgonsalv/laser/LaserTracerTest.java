/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.laser;

import java.awt.Point;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Arrays;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Victor
 */
public class LaserTracerTest {
    LaserTracer lt;
    public LaserTracerTest() {
    }
    private static class GridBuilder{
        private final char[][] grid;
        private final int rows;
        private final int cols;
        public GridBuilder(int rows, int cols) {
            this.rows = rows;
            this.cols = cols;
            grid = new char[rows][cols];
            grid[0][0] ='+';
            grid[0][cols-1] = '+';
            grid[rows-1][0] ='+';
            grid[rows-1][cols-1] = '+';
            Arrays.fill(grid[0], 1, cols-1, '-');
            Arrays.fill(grid[rows-1], 1, cols-1, '-');
            for (int i = 1; i < rows-1; i++) {
                grid[i][0] = '|';
                grid[i][cols-1] = '|';
            }
        }
        public GridBuilder addMirror(int row,int col, char mirror) {
            grid[row][col] = mirror;
            return this;
        }
        
        public Reader getReader() {
            StringBuilder sb = new StringBuilder(rows*(cols+1));
            for (char[] row : grid) {
                sb.append(row);
                sb.append(System.lineSeparator());
            }
            return new StringReader(sb.toString());
        }
         
    }
    
    private static final int UP=0;
    private static final int RIGHT=1;
    private static final int DOWN=2;
    private static final int LEFT=3;
    @Test
    public void testEmpty() throws IOException {
        Reader input = new GridBuilder(4,5).getReader();
        lt = LaserTracer.load(input);
        Point result = lt.getCollision(new Point(2, 1), LEFT);
        Assert.assertEquals(new Point(0, 1), result);
        result = lt.getCollision(new Point(2, 1), UP);
        Assert.assertEquals(new Point(2, 0), result);
        result = lt.getCollision(new Point(2, 1), DOWN);
        Assert.assertEquals(new Point(2, 3), result);
        result = lt.getCollision(new Point(2, 1), RIGHT);
        Assert.assertEquals(new Point(4, 1), result);
    }
    @Test
    public void test1Mirror1() throws IOException {
        Reader input = new GridBuilder(5,5).addMirror(2, 2, '/').getReader();
        lt = LaserTracer.load(input);
        Point result = lt.getCollision(new Point(3, 2), LEFT);
        Assert.assertEquals(new Point(2, 4), result);
        result = lt.getCollision(new Point(2, 3), UP);
        Assert.assertEquals(new Point(4, 2), result);
        result = lt.getCollision(new Point(2, 1), DOWN);
        Assert.assertEquals(new Point(0, 2), result);
        result = lt.getCollision(new Point(1, 2), RIGHT);
        Assert.assertEquals(new Point(2, 0), result);
    }
    @Test
    public void test1Mirror2() throws IOException {
        Reader input = new GridBuilder(6,6).addMirror(2, 2, '\\').getReader();
        lt = LaserTracer.load(input);
        Point result = lt.getCollision(new Point(4, 2), LEFT);
        Assert.assertEquals(new Point(2, 0), result);
        result = lt.getCollision(new Point(2, 4), UP);
        Assert.assertEquals(new Point(0, 2), result);
        result = lt.getCollision(new Point(2, 1), DOWN);
        Assert.assertEquals(new Point(5, 2), result);
        result = lt.getCollision(new Point(1, 2), RIGHT);
        Assert.assertEquals(new Point(2, 5), result);
    }
    @Test
    public void test2Mirrors() throws IOException {
        Reader input = new GridBuilder(6,5).addMirror(2, 2, '\\').addMirror(2, 3, '/').getReader();
        lt = LaserTracer.load(input);
        Point result;
        result = lt.getCollision(new Point(1,2), RIGHT);
        Assert.assertEquals(new Point(0,3), result);
        result = lt.getCollision(new Point(1,3), RIGHT);
        Assert.assertEquals(new Point(0,2), result);
        result = lt.getCollision(new Point(3,3), LEFT);
        Assert.assertEquals(new Point(2,5), result);
        result = lt.getCollision(new Point(3,2), LEFT);
        Assert.assertEquals(new Point(2,0), result);
        result = lt.getCollision(new Point(2,1), DOWN);
        Assert.assertEquals(new Point(4,2), result);
        result = lt.getCollision(new Point(2,4), UP);
        Assert.assertEquals(new Point(4,3), result);
    }
    
}
