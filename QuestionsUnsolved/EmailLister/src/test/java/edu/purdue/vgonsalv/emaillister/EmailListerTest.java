/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.vgonsalv.emaillister;

import java.io.IOException;
import java.io.StringReader;
import java.util.StringJoiner;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.json.JSONException;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

/**
 *
 * @author Victor
 */
public class EmailListerTest {
    EmailLister el;
    StringJoiner sj = new StringJoiner(System.lineSeparator());
    public EmailListerTest() {
    }
    private static JsonArrayBuilder addEmails(String...emails){
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (String email : emails) {
            builder.add(email);
        }
        return builder;
    }
    @Test
    public void testBasic()throws IOException {
        sj.add("google@google.com");
        el = EmailLister.load(new StringReader(sj.toString()));
        String resultString = el.toString();
        JsonReader r = Json.createReader(new StringReader(resultString));
        JsonObject result = r.readObject();
        JsonObject expected = Json.createObjectBuilder().add("google.com", addEmails("google@google.com")).build();
        assertEquals(expected, result);
    }
    @Test
    public void testMultipleSame()throws IOException, JSONException {
        sj.add("google@google.com").add("gmail@google.com").add("youtube@google.com");
        el = EmailLister.load(new StringReader(sj.toString()));
        String result = el.toString();
        JsonObject expected = Json.createObjectBuilder()
                .add("google.com",
                        addEmails("youtube@google.com","google@google.com","gmail@google.com"))
                .build();
        
        JSONAssert.assertEquals(expected.toString(), result,JSONCompareMode.NON_EXTENSIBLE);
    }
    @Test
    public void testMultiple()throws IOException, JSONException {
        sj.add("google@google.com").add("foobar@yahoo.com").add("youtube@google.com")
                .add("baar@yahoo.com").add("microsoft@msn.com").add("gmail@google.com");
        el = EmailLister.load(new StringReader(sj.toString()));
        String result = el.toString();
        JsonObject expected = Json.createObjectBuilder()
                .add("google.com",
                        addEmails("youtube@google.com","google@google.com","gmail@google.com"))
                .add("yahoo.com",
                        addEmails("foobar@yahoo.com","baar@yahoo.com"))
                .add("msn.com", 
                        addEmails("microsoft@msn.com"))
                .build();
        
        JSONAssert.assertEquals(expected.toString(), result,JSONCompareMode.NON_EXTENSIBLE);
    }    
}